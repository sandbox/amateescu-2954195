<?php

namespace Drupal\entity_storage_json\Entity\Storage;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityStorageBase;
use Drupal\Core\Entity\EntityBundleListenerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Schema\DynamicallyFieldableEntityStorageSchemaInterface;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A content entity SQL storage implementation that uses JSON data types.
 *
 * @ingroup entity_api
 */
class JsonContentEntityStorage extends ContentEntityStorageBase implements SqlEntityStorageInterface, DynamicallyFieldableEntityStorageSchemaInterface, EntityBundleListenerInterface {

  /**
   * Active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The name of the table which stores the entity data.
   *
   * @var string
   */
  protected $tableName;

  /**
   * The storage field definitions for this entity type.
   *
   * @var \Drupal\Core\Field\FieldStorageDefinitionInterface[]
   */
  protected $fieldStorageDefinitions;

  /**
   * The mapping of field columns to SQL tables.
   *
   * @var \Drupal\Core\Entity\Sql\TableMappingInterface
   */
  protected $tableMapping;

  /**
   * Name of entity's revision database table field.
   *
   * @var string
   */
  protected $revisionKey;

  /**
   * Constructs a JsonContentEntityStorage object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection to be used.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend to be used.
   */
  public function __construct(EntityTypeInterface $entity_type, Connection $database, EntityManagerInterface $entity_manager, CacheBackendInterface $cache) {
    parent::__construct($entity_type, $entity_manager, $cache);

    $this->database = $database;
    $this->tableName = $entity_type->id();
    $this->fieldStorageDefinitions = $entity_manager->getFieldStorageDefinitions($entity_type->id());

    $this->idKey = $entity_type->getKey('id') ?: 'id';
    $this->revisionKey = $entity_type->getKey('revision') ?: 'revision_id';
    $this->langcodeKey = $entity_type->getKey('langcode') ?: 'langcode';
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('database'),
      $container->get('entity.manager'),
      $container->get('cache.entity')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function onEntityTypeCreate(EntityTypeInterface $entity_type) {
    $table_schema = $this->getEntityTableSchema($entity_type);
    $schema_handler = $this->database->schema();

    if (!$schema_handler->tableExists($this->tableName)) {
      $schema_handler->createTable($this->tableName, $table_schema);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onEntityTypeUpdate(EntityTypeInterface $entity_type, EntityTypeInterface $original) {
    // Updating the entity type definition does not affect the schema.
  }

  /**
   * {@inheritdoc}
   */
  public function onEntityTypeDelete(EntityTypeInterface $entity_type) {
    $schema_handler = $this->database->schema();

    if ($schema_handler->tableExists($this->tableName)) {
      $schema_handler->dropTable($this->tableName);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function doLoadMultiple(array $ids = NULL) {
    // @todo
  }

  /**
   * {@inheritdoc}
   */
  protected function doLoadRevisionFieldItems($revision_id) {
    @trigger_error('"\Drupal\Core\Entity\ContentEntityStorageBase::doLoadRevisionFieldItems()" is deprecated in Drupal 8.5.x and will be removed before Drupal 9.0.0. "\Drupal\Core\Entity\ContentEntityStorageBase::doLoadMultipleRevisionsFieldItems()" should be implemented instead. See https://www.drupal.org/node/2924915.', E_USER_DEPRECATED);

    $revisions = $this->doLoadMultipleRevisionsFieldItems([$revision_id]);

    return !empty($revisions) ? reset($revisions) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function doLoadMultipleRevisionsFieldItems($revision_ids) {
    // @todo
  }

  /**
   * {@inheritdoc}
   */
  protected function doSaveFieldItems(ContentEntityInterface $entity, array $names = []) {
    $full_save = empty($names);
    $update = !$full_save || !$entity->isNew();

    if ($update) {
      $new_revision = $full_save && $entity->isNewRevision();
      $this->saveToTable($entity, $new_revision);
    }
    else {
      $this->saveToTable($entity);
    }
  }

  /**
   * Saves fields that use the shared tables.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity object.
   * @param bool $new_revision
   *   (optional) Whether we are dealing with a new revision. By default fetches
   *   the information from the entity object.
   */
  protected function saveToTable(ContentEntityInterface $entity, $new_revision = FALSE) {
    // Since we don't have any auto-incremented column, we need to find the new
    // ID and revision ID manually.
    $query = $this->database->select($this->tableName, 'base_table');
    $query->addExpression("COALESCE(MAX({$this->idKey}), 1)", $this->idKey);
    $query->addExpression("COALESCE(MAX({$this->revisionKey}), 1)", $this->revisionKey);
    $result = $query->execute()->fetchAssoc();

    // @todo
  }

  /**
   * {@inheritdoc}
   */
  protected function doDeleteFieldItems($entities) {
    // @todo
  }

  /**
   * {@inheritdoc}
   */
  protected function doDeleteRevisionFieldItems(ContentEntityInterface $revision) {
    // @todo
  }

  /**
   * {@inheritdoc}
   */
  protected function readFieldItemsToPurge(FieldDefinitionInterface $field_definition, $batch_size) {
    // @todo
  }

  /**
   * {@inheritdoc}
   */
  protected function purgeFieldItems(ContentEntityInterface $entity, FieldDefinitionInterface $field_definition) {
    // @todo
  }

  /**
   * {@inheritdoc}
   */
  public function countFieldData($storage_definition, $as_bool = FALSE) {
    // @todo
  }

  /**
   * {@inheritdoc}
   */
  public function getTableMapping(array $storage_definitions = NULL) {
    $table_mapping = $this->tableMapping;

    // If we are using our internal storage definitions, which is our main use
    // case, we can statically cache the computed table mapping. If a new set
    // of field storage definitions is passed, for instance when comparing old
    // and new storage schema, we compute the table mapping without caching.
    if (!isset($this->tableMapping) || $storage_definitions) {
      $definitions = $storage_definitions ?: $this->entityManager->getFieldStorageDefinitions($this->entityTypeId);
      $table_mapping = new JsonTableMapping($this->entityType, $definitions, $this->tableName);

      // Cache the computed table mapping only if we are using our internal
      // storage definitions.
      if (!$storage_definitions) {
        $this->tableMapping = $table_mapping;
      }
    }

    return $table_mapping;
  }

  /**
   * {@inheritdoc}
   */
  protected function has($id, EntityInterface $entity) {
    return !$entity->isNew();
  }

  /**
   * {@inheritdoc}
   */
  public function requiresEntityStorageSchemaChanges(EntityTypeInterface $entity_type, EntityTypeInterface $original) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function requiresEntityDataMigration(EntityTypeInterface $entity_type, EntityTypeInterface $original) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function requiresFieldStorageSchemaChanges(FieldStorageDefinitionInterface $storage_definition, FieldStorageDefinitionInterface $original) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function requiresFieldDataMigration(FieldStorageDefinitionInterface $storage_definition, FieldStorageDefinitionInterface $original) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function finalizePurge(FieldStorageDefinitionInterface $storage_definition) {}

  /**
   * {@inheritdoc}
   */
  public function onBundleCreate($bundle, $entity_type_id) {}

  /**
   * {@inheritdoc}
   */
  public function onBundleDelete($bundle, $entity_type_id) {}

  /**
   * {@inheritdoc}
   */
  protected function getQueryServiceName() {
    return 'entity.query.sql';
  }

  /**
   * Returns the database schema for the entity table.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   An entity type object.
   *
   * @return array
   *  A schema array for the entity table.
   */
  protected function getEntityTableSchema(EntityTypeInterface $entity_type) {
    $entity_type_id = $entity_type->id();

    // Get the column schema for the ID field.
    $id_schema = $this->fieldStorageDefinitions[$this->idKey]->getColumns()[$this->fieldStorageDefinitions[$this->idKey]->getMainPropertyName()];

    // Get the column schema for the revision ID field and fall back to the ID
    // field schema if the entity type does not have a 'revision' entity key.
    if ($entity_type->hasKey('revision')) {
      $revision_schema = $this->fieldStorageDefinitions[$this->revisionKey]->getColumns()[$this->fieldStorageDefinitions[$this->revisionKey]->getMainPropertyName()];
    }
    else {
      $revision_schema = $id_schema;
    }

    // Get the column schema for the language code field and fall back to the
    // schema of the 'language' field type if the entity type does not have a
    // 'langcode' entity key.
    if ($entity_type->hasKey('langcode')) {
      $langcode_schema = $this->fieldStorageDefinitions[$this->langcodeKey]->getColumns()[$this->fieldStorageDefinitions[$this->langcodeKey]->getMainPropertyName()];
    }
    else {
      $storage_definition = BaseFieldDefinition::create('language');
      $langcode_schema = $storage_definition->getColumns()[$storage_definition->getMainPropertyName()];
    }

    $schema = [
      'description' => "The data table for $entity_type_id entities.",
      'fields' => [
        $this->idKey => $id_schema + ['not null' => TRUE],
        $this->revisionKey => $revision_schema + ['not null' => TRUE],
        $this->langcodeKey => $langcode_schema + ['not null' => TRUE],
        // @todo We should probably add a column for the entity bundle.
        'data' => [
          'type' => 'text',
          'mysql_type' => 'json',
          'pgsql_type' => 'jsonb',
          'size' => 'medium',
          'not null' => TRUE,
        ],
      ],
      'primary key' => [$this->idKey, $this->revisionKey, $this->langcodeKey],
      'indexes' => [
        // @todo Add an index for the default langcode and another one for the
        //   default revision.
        //$entity_type_id . '__id__default_langcode__langcode' => [$this->idKey, $entity_type->getKey('default_langcode'), $entity_type->getKey('langcode')],
      ],
    ];

    return $schema;
  }

}
