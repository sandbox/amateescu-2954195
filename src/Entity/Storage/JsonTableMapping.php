<?php

namespace Drupal\entity_storage_json\Entity\Storage;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\DefaultTableMapping;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines a table mapping for entities stored in the JSON storage.
 */
class JsonTableMapping extends DefaultTableMapping {

  /**
   * The name of the table which stores the entity data.
   *
   * @var string
   */
  protected $tableName;

  /**
   * Constructs a JsonTableMapping.
   *
   * @param \Drupal\Core\Entity\ContentEntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Field\FieldStorageDefinitionInterface[] $storage_definitions
   *   A list of field storage definitions that should be available for the
   *   field columns of this table mapping.
   * @param string $table_name
   *   The name of the table which stores the entity data.
   */
  public function __construct(ContentEntityTypeInterface $entity_type, array $storage_definitions, $table_name) {
    parent::__construct($entity_type, $storage_definitions);
    $this->tableName = $table_name;
  }

  /**
   * {@inheritdoc}
   */
  public function getTableNames() {
    return [$this->tableName];
  }

  /**
   * {@inheritdoc}
   */
  public function getAllColumns($table_name) {
    if (!isset($this->allColumns[$table_name])) {
      $this->allColumns[$table_name] = [];

      foreach ($this->getFieldNames($table_name) as $field_name) {
        $this->allColumns[$table_name] = array_merge($this->allColumns[$table_name], array_values($this->getColumnNames($field_name)));
      }
    }
    return $this->allColumns[$table_name];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldNames($table_name) {
    return array_keys($this->fieldStorageDefinitions);
  }

  /**
   * {@inheritdoc}
   */
  public function setFieldNames($table_name, array $field_names) {
    // Nothing to do here.
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getExtraColumns($table_name) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function setExtraColumns($table_name, array $column_names) {
    // Nothing to do here.
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function allowsSharedTableStorage(FieldStorageDefinitionInterface $storage_definition) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function requiresDedicatedTableStorage(FieldStorageDefinitionInterface $storage_definition) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDedicatedTableNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getReservedColumns() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldColumnName(FieldStorageDefinitionInterface $storage_definition, $property_name) {
    $field_column_name = NULL;
    $field_name = $storage_definition->getName();

    $dedicated_columns = [
      $this->entityType->getKey('id'),
      $this->entityType->hasKey('revision') ? $this->entityType->getKey('revision') : 'revision_id',
      $this->entityType->hasKey('langcode') ? $this->entityType->getKey('langcode') : 'langcode',
    ];

    if (in_array($field_name, $dedicated_columns, TRUE)) {
      $field_column_name = $field_name;
    }
    else {
      $field_column_name = "data->>$.$field_name.$property_name";
    }

    return $field_column_name;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldTableName($field_name) {
    return $this->tableName;
  }

}
